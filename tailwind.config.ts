import type { Config } from "tailwindcss";
import { nextui } from "@nextui-org/react";


const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",

  ],
  theme: {
    extend: {
      colors: {
        white: "#FFFFFF",
        black: "#33363B",
        lightGray: "#F4F6F8",
        darkGray: "#E3E8EE",
        gray: "#8B98A5",
        primary: "#21CDA8",
        background: "#EDEDED",
        mainGray:"#E3E5E8"
      },
      fontSize: {
        xs: "0.75rem", //12px
        sm: "0.875rem", //14px
        md: "1rem", //16px
        ml: "1.125rem", //18px
        lg: "1.25rem", //20px
        xl: "1.5rem", //24px
        "2xl": "2rem", //32px
        "3xl": "3rem", //48px
        "4xl": "4rem", //64px
      },
      fontFamily: {
        bold: ["Bold"],
        regular: ["Regular"],
        ttRegular: ["TTRegular"],
        ttBold: ["TTBold"],
        ttDemibold: ["TTDemibold"],
    },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [nextui()],
};
export default config;
