// ./sanity/lib/queries.ts

import { groq } from "next-sanity";

export const POSTS_QUERY = groq`*[_type == "post"] | order(EregteiOnoo + EmegteiOnoo)  {
    _id,
    slug,
    title,
    teamColor,
    draftNumber,
    EregteiOnoo,
    EmegteiOnoo,
    teamMeneger,
    teamOwner,
    _createdAt,
    publishedAt,
    mainImage {
      ...,
      "blurDataURL":asset->metadata.lqip,
      "ImageColor": asset->metadata.palette.dominant.background,
    }
  }`;

export const POST_QUERY = groq`*[_type == "post" && slug.current == $slug][0]`;