import client from "./client";
import imageUrlBuilder from "@sanity/image-url";

const builder = imageUrlBuilder(client);

export const urlFor = (source) => {
	return builder.image(source);
};
export const getPost = async () => {
	return await client.fetch(
		`
*[_type == "post"] | order(EregteiOnoo + EmegteiOnoo)  {
  _id,
  slug,
  title,
  draftNumber,
  EregteiOnoo,
  EmegteiOnoo,
  teamMeneger,
  teamOwner,
  _createdAt,
  publishedAt,
  mainImage {
    ...,
    "blurDataURL":asset->metadata.lqip,
    "ImageColor": asset->metadata.palette.dominant.background,
  }
}
`
		)
}



