import { SanityDocument } from "next-sanity";
import { loadQuery } from "../../sanity/lib/store";
import { POSTS_QUERY } from "../../sanity/lib/queries";
// import MLeaderBoardTable from "./components/MLeaderBoardTable";
import MHeader from "./components/MHeader";
import MChips from "./components/MChips";
import dynamic from 'next/dynamic'
import MFooter from "./components/MFooter";
import TawkTo from "./components/TawkTo";

// import MSocialLink from "./components/MSocialLink";
export default async function Page() {
  const initial = await loadQuery<SanityDocument[]>(POSTS_QUERY);
  const MLeaderBoardTable = dynamic(() => import('./components/MLeaderBoardTable'), {
    loading: () => <p>Loading...</p>,
  })

  return <div className="">
  <MHeader/>

  <MLeaderBoardTable posts={initial.data}/>
  <div className='mx-auto w-48 '>
  <p className=" text-white font-bold text-ml mt-10  ">
  Тэмцээнүүд
        </p>
</div>
<MChips/>
<MFooter/>

<TawkTo />


  </div>

}