import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { UiProvider } from "../../provider/uiProvider";
import { Analytics } from "@vercel/analytics/react"
import TawkTo from "../app/components/TawkTo"

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "M leader board",
  description: "М Банкны тэмцээнүүдтэй холбоотой мэдээлэл",
  openGraph: {
    title: 'My Page Title',
    description: 'My page description.',
    images: [
      {
        url: '/Logo.png',
        alt: 'An alt text description of the image',
        width: 800,
        height: 600,
      },
    ],
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
      <html lang="en">
      <body  className={`${inter.className} bg-[#101010]`}>
        <Analytics/>

      <UiProvider>{children}</UiProvider>

      </body>
    </html>
  );
}
