import React from 'react'

export default function page() {
  return (
  <div>
  <div className="flex ">
    <div className="w-72 m-8 text-center text-xl">Quarterfinals Round</div>
    <div className="w-72 m-8 text-center text-xl">Semifinals Round</div>
    <div className="w-72 m-8 text-center text-xl">Final Round</div>
  </div>
  <div className="flex items-center  ">
    <div className="flex-col m-4 ">
      {/* <div className="bg-gray w-72 h-20 m-4 rounded-lg" /> */}
      <div className="flex bg-gray w-72 h-14 m-4 rounded-lg flex-col-reverse divide-y divide-y-reverse">
  <div>01</div>
  <div>02</div>
</div>
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
    </div>
    <div className="flex-col m-4">
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
      <div className="p-10" />
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
    </div>
    <div className="flex-col m-4">
      <div className="bg-gray w-72 h-20 m-4 rounded-lg" />
    </div>
  </div></div>

  )
}
