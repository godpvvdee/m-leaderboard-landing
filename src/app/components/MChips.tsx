'use client'
import React, { useState } from "react";
import {Chip, Avatar} from "@nextui-org/react";
import MTimeSchedule from "./MTimeSchedule";
import MBilliardTime from "./MBilliardTime";

export default function MChips() {
  const [value ,setValue ] = useState("Basketball");
    const arr =[{
    name:"Basketball",
    img:"colored_Basketball.png",
    img2:"colored_Basketball.png",

  },{
    name:"Darts",
    img:"colored_Darts.png",
    img2:"disabled_Darts.png",
  },{
    name:"Snooker",
    img:"colored_Billiard.png",

    img2:"disabled_Billiard.png",
  },{
    name:"Table tennis",
    img:"colored_Billiard.png",

    img2:"disabled_Table_tennis.png",
  },{
    name:"Cs 2",
    img:"colored_Billiard.png",

    img2:"disabled_CS.png",
  },{
    name:"Dota 2",
    img:"colored_Billiard.png",

    img2:"disabled_Dota.png",
  },{
    name:"Volleyball",
    img:"colored_Billiard.png",

    img2:"disabled_Volleyball.png",
  },{
    name:"Chess",
    img:"colored_Billiard.png",

    img2:"disabled_Chess.png",
  },{
    name:"Foosball",
    img:"colored_Billiard.png",

    img2:"disabled_Foosball.png",
  }]
  console.log("value",value)
  return (
    <>
<div className=" cursor-pointer inline-block md:flex sm:flex lg:flex ml-14 md:m-0 sm:m-0 lg:m-0  justify-center items-center">

{arr.map((e,index) => (
            <>
            <button
            onClick={ () =>setValue(e.name)}
            key={index} type="button">
  <span className={`${e.name == value ? " border-primary text-white " : "border-gray text-gray"} inline-flex  py-2.5 px-3 items-center justify-between text-xs capitalize border rounded-full hover:bg-gray-gray1 mb-2 mr-2 `}>
  <img alt=" " src={ `${e.name == value ? `/m_cup2024/${e.img}` : `/m_cup2024/${e.img2}`}`} decoding="async" data-nimg={1} className="object-cover w-4 h-4 mr-1" loading="lazy" style={{color: 'transparent'}} width={16} height={16} />

    <span className="capitalize lg:inline leading-5">{e.name}</span></span>
</button>
            </>
          ))}

      <div className="py-10">
        </div>
    </div>
    {value == "Snooker" && <MBilliardTime/>}

    {value == "Darts" && <MTimeSchedule/>}

    
    </>
  );
}
