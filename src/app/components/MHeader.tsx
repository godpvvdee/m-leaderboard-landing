'use client'
import React from 'react'
import Image from 'next/image'

export default function MHeader() {
  return (
    <>
    <div className='flex py-4 justify-center items-center'>

    <Image
        alt="logo"
        src={"/Logo.png"}
        // placeholder="blur"
        width={140}
        height={62}
        />
    </div>
      {/* <div className='mx-auto w-72 py-4'> */}
      <div className='flex  justify-center items-center'>
      <h1 className="my-4 text-ml font-bold text-white  ">

      Хүснэгтийн мэдээлэл
      </h1>
      </div>
      </>
  )
}
