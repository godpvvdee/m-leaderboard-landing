'use client'
import { SanityDocument } from "next-sanity";
import Image from "next/image";
export default function MLeaderBoardTable({ posts }: { posts: SanityDocument[] }) {
   
    return (
        <>
          <div>
            <table className="w-9/12 border-collapse border border-primary text-white font-regular mx-auto text-sm text-left  ">

            <thead className="text-xs uppercase">
      <tr>
        <th scope="col" className="p-4">
          <div className="flex items-center">RANK</div>
        </th>
        <th scope="col" className="px-3 py-3">TEAM NAME</th>
        <th scope="col" className="px-3   py-3">PLAYERS</th>
        <th scope="col" className="px-6 py-3">POINTS</th>
      </tr>
    </thead>
              {/* {posts.map((post,index) =>{
              
          })} */}
      
              <tbody>
                {posts.length > 0 &&
                  posts.map((item, index) => (
                    <tr
                      key={index}
                      className={`${index % 2 == 0 ? "bg-[#101010]" : "bg-[#0A0A0A]"}`}
                    >
                      <td className={`${index == 0 ? 'bg-[#3A2F05]' : index ==1 ? "bg-[#292E37]" :index ==2 ? "bg-[#271304]" :  "" } w-4 p-6 font-bold `} >
                        {index  == 0 ? (
                          <Image
                            src="/Gold.png"
                            width={100}
                            height={100}
                            alt="alt"
                          />
                        ) : index  == 1? (
                          <Image
                            src="/m_cup2024/Silver.png"
                            width={100}
                            height={100}
                            alt="mongo"
                          />
                        ) : index  === 2 ? (
                          <Image
                            src="/Bronze.png"
                            width={100}
                            height={100}
                            alt="hvrel"
                          />
                        ) : (
                          index + 1
                        )}
                      </td>
                      <td className={`${index == 0 ? 'bg-[#3A2F05]' : index ==1 ? "bg-[#292E37]" :index ==2 ? "bg-[#271304]" :  "" } `} >

                      <div className="flex">
                        {item.teamColor && (
                          <div
                            className={`rounded-full mt-2 w-10 h-10 ${item.teamColor}`}
                          >
                            {/* {item.draftNumber} */}
                          </div>
                        )}
      
                        {/* <img className="w-10 h-10 rounded-full" src="team1.png" alt="Jese image" /> */}
                        <div className="ps-3  ">
                          <div className="text-ml  mt-1 font-regular">{item.title}</div>
                          <div className="font-normal  text-[#B5B4B4]">
                          <p className="text-[#B5B4B4] font-regular text-sm ">
                            {item.teamOwner} , {item.teamMeneger}
                            </p>
                          </div>
                        </div>
                      </div>
                      </td>

                      <td className={`${index == 0 ? 'bg-[#3A2F05]' : index ==1 ? "bg-[#292E37]" :index ==2 ? "bg-[#271304]" :  "" } px-6 py-4  `} >

                     28</td>
                      <td className={`${index == 0 ? 'bg-[#3A2F05]' : index ==1 ? "bg-[#292E37]" :index ==2 ? "bg-[#271304]" :  "" } px-6 py-4 `}>
                        <div className="flex items-center">
                          <div className={`h-2.5 w-2.5 rounded-full  me-2`} />{" "}
                          {item.EmegteiOnoo + item.EregteiOnoo}
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </>
      );
      
}