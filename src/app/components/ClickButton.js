// components/RunawayButton.js
'use client'

import { useState } from 'react';
import anime from 'animejs';
import Image from 'next/image';

const RunawayButton = () => {
  const [isMoving, setIsMoving] = useState(false);

  const animateMove = (element, prop, pixels) =>
    anime({
      targets: element,
      [prop]: `${pixels}px`,
      easing: "easeOutCirc"
    });

  const handleMove = (event) => {
    const button = event.currentTarget;
    const top = getRandomNumber(window.innerHeight - button.offsetHeight);
    const left = getRandomNumber(window.innerWidth - button.offsetWidth);

    animateMove(button, "left", left).play();
    animateMove(button, "top", top).play();
  };

  const getRandomNumber = (num) => {
    return Math.floor(Math.random() * (num + 1));
  };

  return (
    <>
     <button
      className="absolute  top-1/4 left-1/4 transform -translate-x-1/2 -translate-y-1/2 h-16 w-40 text-lg rounded-lg shadow-md bg-black"
      onClick={()=>setIsMoving(true)}
    >
      Yes 
    </button>
    <Image width={500} height={500}  src={`${isMoving ? "https://media.tenor.com/Sn4acC1CDtUAAAAM/kristigocouple2024.gif" : "https://media.tenor.com/w6DS9_ISMc0AAAAM/baby-cute.gif"} `}/>

    
    <button
      id="runaway-btn"
      className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 h-16 w-40 text-lg rounded-lg shadow-md bg-black"
      onMouseOver={handleMove}
      onClick={handleMove}
    >
      No
    </button>
   
    </>
  );
};

export default RunawayButton;
