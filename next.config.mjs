/** @type {import('next').NextConfig} */
import createMDX from '@next/mdx'

const nextConfig = {
  pageExtensions: ['js', 'jsx', 'md', 'mdx', 'ts', 'tsx'],

    images: {
      domains: ['media.tenor.com'],

      remotePatterns: [
        {
          protocol: "https",
          hostname: "cdn.sanity.io",
        },
      ],
    },
    experimental: {
      taint: true,
    },
    // ...other config settings
  };
  const withMDX = createMDX({
    // Add markdown plugins here, as desired
  })
  export default withMDX(nextConfig)

  
  // export default nextConfig;